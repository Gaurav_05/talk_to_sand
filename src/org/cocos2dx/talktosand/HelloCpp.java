/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.talktosand;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import twitter4j.TwitterException;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class HelloCpp extends Cocos2dxActivity {

	FrameLayout framelayout;
	static Context context;
	static NetworkInfo netInfo, netinfof;
	static ActivityManager am;
	private static Activity me = null;
	private static SharedPreferences prefs;
	static ConnectivityManager cm;
	// private static Handler handler;
	static ProgressDialog progress;
	private final static Handler mTwitterHandler = new Handler();
	private final static Handler handler = new Handler();;
	static Bitmap bmp;
	static Cocos2dxGLSurfaceView glSurfaceView;
	public static final String TWEET_URL = "http://nameagrainofsand.com";
	private static final String FACEBOOK_APPID = "487059177996871";
	private static final String FACEBOOK_PERMISSION = "publish_stream";
	private static final String TAG = "TALK TO SAND";
	private static final String MSG = "Message from TALK TO SAND";
	private static FacebookConnector facebookConnector;
	List<ApplicationInfo> packages;

	final static Runnable mUpdateTwitterNotification = new Runnable() {
		public void run() {

			Toast.makeText(context, "Tweet sent !", Toast.LENGTH_LONG).show();
		}
	};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		me = this;
		HelloCpp.prefs = PreferenceManager.getDefaultSharedPreferences(this);
		bmp = BitmapFactory.decodeResource(HelloCpp.this.getResources(),
				R.drawable.twitter);
		progress = new ProgressDialog(this);
		context = HelloCpp.this;
		HelloCpp.facebookConnector = new FacebookConnector(FACEBOOK_APPID,
				this, getApplicationContext(),
				new String[] { FACEBOOK_PERMISSION });
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();

		StrictMode.setThreadPolicy(policy);

		cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

	}

	public Cocos2dxGLSurfaceView onCreateView() {
		glSurfaceView = new Cocos2dxGLSurfaceView(this);

		glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);

		return glSurfaceView;

	}

	static {
		System.loadLibrary("cocos2dcpp");

	}

	public static void openURL(String url) {

		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		me.startActivity(i);
	}

	public static void alertBack() {

		Runnable runnable4 = new Runnable() {
			@Override
			public void run() {
				handler.post(new Runnable() { // This thread runs in the UI
					@Override
					public void run() {
						alert();
					}
				});
			}
		};
		new Thread(runnable4).start();

	}

	static public void alert() {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle("Quit");

		// set dialog message
		alertDialogBuilder
				.setMessage("Do you want to quit?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity

								((Activity) context).finish();

							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		String packagename = "org.cocos2dx.talktosand";
		am.killBackgroundProcesses(packagename);
	}

	static public boolean isInternetConnection() {
		Log.v("InternetConnection", "isInternetConnection Start");
		/*
		 * //Toast make fail DEMO ! :S Toast toast1 =
		 * Toast.makeText(myAndroidContext, "Checking Internet",
		 * Toast.LENGTH_SHORT); toast1.show();
		 */
		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conMgr.getActiveNetworkInfo();
		if (i == null) {
			Log.v("InternetConnection", "isInternetConnection NULL :S");
			return false;
		}
		if (!i.isConnected()) {
			Log.v("InternetConnection", "isInternetConnection is not connected");
			return false;
		}
		if (!i.isAvailable()) {
			Log.v("InternetConnection", "isInternetConnection is not available");
			return false;
		}
		Log.v("InternetConnection", "isInternetConnection DONE!");
		return true;
	}

	static public void sendATweet() {
		Runnable runnable3 = new Runnable() {
			@Override
			public void run() {
				handler.post(new Runnable() { // This thread runs in the UI
					@Override
					public void run() {
						netInfo = cm.getActiveNetworkInfo();
						if (netInfo == null) {

							Toast.makeText(context,
									"No network connection available",
									Toast.LENGTH_LONG).show();
						} else {
							tweet();
						}
					}
				});
			}
		};
		new Thread(runnable3).start();

	}

	public static void tweet() {

		if (TwitterUtils.isAuthenticated(prefs)) {
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					handler.post(new Runnable() { // This thread runs in the UI
						@Override
						public void run() {
							progress(); // Update the UI
						}
					});
				}
			};
			new Thread(runnable).start();

			sendTweet();
		} else {

			Intent i = new Intent(context, PrepareRequestTokenActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			try {
				i.putExtra("tweet_msg", getTweetMsg());
				i.putExtra("tweet_msg1", bmp);
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			context.startActivity(i);
		}
	}

	public static void progress() {
		// TODO Auto-generated method stub

		progress.setCancelable(true);
		progress.setMessage("Sending Tweet");
		progress.setMax(100);

		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

		progress.show();

	}

	public static void sendTweet() {

		Thread t = new Thread() {
			public void run() {
				try {

					TwitterUtils.sendTweet(prefs, getTweetMsg(), bmp);

					mTwitterHandler.post(mUpdateTwitterNotification);
					progress.dismiss();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
		t.start();
	}

	private static String getTweetMsg() throws TwitterException {

		File file = new File(
				"android.resource://org.cocos2dx.hellocpp/drawable/twitter.png");
		System.out.println(file);

		return "Talk To The Sand " + TWEET_URL;

	}

	static public void postFbMessage() {
		Runnable runnable3 = new Runnable() {
			@Override
			public void run() {
				handler.post(new Runnable() { // This thread runs in the UI
					@Override
					public void run() {
						netInfo = cm.getActiveNetworkInfo();
						if (netInfo == null) {

							Toast.makeText(context,
									"No network connection available",
									Toast.LENGTH_LONG).show();
						} else {
							postMessage();
						}
					}
				});
			}
		};
		new Thread(runnable3).start();
	}

	private static void postMessage() {
		if (facebookConnector.getFacebook().isSessionValid()) {

			Runnable runnable2 = new Runnable() {
				@Override
				public void run() {
					handler.post(new Runnable() { // This thread runs in the UI
						@Override
						public void run() {
							postMessageInThread(); // Update the UI
						}
					});
				}
			};
			new Thread(runnable2).start();

		} else {
			SessionEvents.AuthListener listener = new SessionEvents.AuthListener() {

				public void onAuthSucceed() {

					postMessageInThread();
				}

				public void onAuthFail(String error) {

				}
			};
			SessionEvents.addAuthListener(listener);
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					handler.post(new Runnable() { // This thread runs in the UI
						@Override
						public void run() {
							facebookConnector.login(); // Update the UI
						}
					});
				}
			};
			new Thread(runnable).start();
		}

	}

	@SuppressWarnings("deprecation")
	private static void postMessageInThread() {

		Bundle parameters = new Bundle();
		parameters.putString("description", "http://nameagrainofsand.com");
		parameters.putString("link", "http://nameagrainofsand.com");
		parameters.putString("name", "Talk to the Sand");
		parameters.putString("caption", "Demo");

		facebookConnector.getFacebook().dialog(context, "stream.publish",
				parameters, new DialogListener() {

					public void onError(DialogError arg0) {
					}

					public void onComplete(Bundle arg0) {
					}

					public void onCancel() {
					}

					@Override
					public void onFacebookError(FacebookError e) {
						// TODO Auto-generated method stub

					}

				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		this.facebookConnector.getFacebook().authorizeCallback(requestCode,
				resultCode, data);
	}

	private String getFacebookMsg() {
		return MSG + " at " + new Date().toLocaleString();
	}

}
